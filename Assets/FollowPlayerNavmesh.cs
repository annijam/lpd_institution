﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

[RequireComponent(typeof(NavMeshAgent))]
public class FollowPlayerNavmesh : MonoBehaviour {

    [Range(0f, 10f)]
    [SerializeField]
    private float closeEnoughDistance = 5f;

    private GameObject _player;
    private NavMeshAgent _navMeshAgent;
    private Animator anim;
    int attackHash = Animator.StringToHash("Close");

    // Use this for initialization
    void Start () {
        _player = GameObject.FindGameObjectWithTag("Player"); //Find player by tag. It can be assigned in game object inspector tag section (below name).
        _navMeshAgent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
        if (Vector3.Distance(transform.position, _player.transform.position) > closeEnoughDistance) // check if distance between player and gameobject is greater than close enough value
        {
            PerformFollowPlayer();
            //anim.SetTrigger(attackHash);
            anim.SetFloat("Distance", Vector3.Distance(transform.position, _player.transform.position));
        }
    }

    private void PerformFollowPlayer()
    {
        _navMeshAgent.SetDestination(_player.transform.position);
        //anim.SetFloat("Distance", Vector3.Distance(transform.position, _player.transform.position));
    }
}
