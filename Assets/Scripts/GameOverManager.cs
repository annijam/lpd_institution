﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverManager : MonoBehaviour {
    public PlayerHealth playerHealth;      
    Animator anim;                       

    // Use this for initialization
    void Start () {
        anim = GetComponent<Animator>();
    }
	
	// Update is called once per frame
	void Update () {
     
        if (playerHealth.currentHealth <= 0 || (playerHealth.currentHealth > 0 && KeyManager.keyCount == 5))
        {
            // ... tell the animator the game is over.
            anim.SetTrigger("GameOver");
            StartCoroutine(Waiting());
        
        }
        
    }
    private IEnumerator Waiting()
    {
        yield return new WaitForSeconds(7f);
        Debug.Log("pagaja 7 sek");
        yield return null;
        SceneManager.LoadScene(0);
    }
}
