﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class wepScript : MonoBehaviour {

    [SerializeField]
    private float speed = 5000f;

    [SerializeField]
    private GameObject bulletPrefab;

    [SerializeField]
    private Transform bulletSpawn;

    private AudioSource aS;
    private Animator an;
    public AudioClip shoot;
    public AudioClip empty;
    public AudioClip reload;

    int bulletCount;
    int maxBullet=40;
    bool reloading = false;
    

    // Use this for initialization
    private void Start()
    {
        an = GetComponent<Animator>();
        aS = GetComponent<AudioSource>();
        bulletCount = maxBullet;
        

    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetMouseButtonDown(0) && (bulletCount>0))//(Input.GetKey(KeyCode.Mouse0))
        {
            Shoot();
           
        }
        if (Input.GetMouseButtonDown(0) && (bulletCount == 0))
        {
            an.CrossFadeInFixedTime("MachineGin_shoot", 0.2f);
            aS.PlayOneShot(empty);
        }
        if (Input.GetKey(KeyCode.Keypad1))
        {
            Reload();
            reloading = false;
        }
    }

    private void Shoot()
    {
        if (reloading)
        {
            return;
        }
        else
        {
            an.CrossFadeInFixedTime("MachineGin_shoot", 0.2f);
            aS.PlayOneShot(shoot);
            bulletCount--;
            //bulletPrefab.rigidbody.AddForce(bulletSpawn.forward * speed);
           // Instantiate(bulletPrefab, bulletSpawn.forward, transform.rotation);
            // use spawnPoint forward direction to shoot:
            var Bullet = Instantiate(bulletPrefab, bulletSpawn.position, bulletSpawn.rotation);
            Bullet.GetComponent<Rigidbody>().AddForce(bulletSpawn.forward * 2000);
        }
    }
    private void Reload()
    {
        reloading = true;
        an.Play("MachineGun_reload");
        aS.PlayOneShot(reload);
        bulletCount = maxBullet;
    }
}
