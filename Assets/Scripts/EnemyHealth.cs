﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealth : MonoBehaviour {
    public int startingHealth = 100;            // The amount of health the enemy starts the game with.
    public int currentHealth;                   // The current health the enemy has.
    //public float sinkSpeed = 2.5f;              // The speed at which the enemy sinks through the floor when dead.
    public AudioClip hurt;                 // The sound to play when the enemy dies.
    public bool isDead =false; 
    Animator anim;                              // Reference to the animator.
    AudioSource enemyAudio;                     // Reference to the audio source.
                     
    bool isSinking;                             // Whether the enemy has started sinking through the floor.
    public int damage =20;
    Rigidbody rb;


    void Start () {
        anim = GetComponent<Animator>();
        enemyAudio = GetComponent<AudioSource>();
        currentHealth = startingHealth;
        rb = GetComponent<Rigidbody>();
    }

	void Update () {
        
    }
    public void TakeDamage(int amount)//, Vector3 hitPoint)
    {
        if (isDead)
            return;
        currentHealth -= amount;
        Debug.Log("health" + currentHealth);
        
        if(currentHealth <= 0)
        {
            Death();

        }
    }

    void Death()
    {
        isDead = true;  //lai nepiecelas un nemirst atkal
        anim.SetTrigger("Dead");
        this.GetComponent<FollowPlayerNavmesh>().enabled = false;  //lai neizpildas vairs follow skripts ja miris 
        Destroy(gameObject, 2f);
        //enemyAudio.clip = deathClip;
        //enemyAudio.Play();
    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Bullet")
        {
            Debug.Log("trapits  enemy");
            enemyAudio.PlayOneShot(hurt);
            TakeDamage(damage);
            Destroy(collision.gameObject);
        }
    }
}
