﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WinText : MonoBehaviour {
    Text text;
    public PlayerHealth playerHealth;
    // Use this for initialization
    void Start () {
        text = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        if (playerHealth.currentHealth <= 0)
        {
            text.text = "Game Over";
        }
        else
        {
            text.text = "YOU win";
        }
    }
}
