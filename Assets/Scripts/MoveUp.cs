﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveUp : MonoBehaviour {
    public Transform target;
    public float speed;

    // Use this for initialization
    void Start () {
        /*if (transform.position.y < 30f)
        {
            transform.Translate(Vector3.up * Time.deltaTime, Space.World);
        }*/
    }
	
	// Update is called once per frame
	void Update () {
        if (transform.position.y < 60f)
        {
            transform.Translate(Vector3.up * Time.deltaTime * 3, Space.World);
        }

        Vector3 targetDir = target.position - transform.position;    
        float step = speed * Time.deltaTime;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, step, 0.0F);  //novero player
        transform.rotation = Quaternion.LookRotation(newDir);
    }
}
